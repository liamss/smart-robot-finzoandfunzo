double Setpoint = 2.0, Input, Output;

double Kp=1.175, Ki=0.043, Kd=0.082;

double KpR= Kp * 1.5, KiR= Ki * 7.5, KdR= Kd * 2;

double pid_min_output = -100.0, pid_max_output = 100.0;