class MotorDriver
{
	public:
		MotorDriver(uint8_t, uint8_t, uint8_t, uint8_t, uint8_t);

		void init(void);

		void drive_motor(Kinematics, Kinematics::output);
		void stop_motor(void);

	private:
		uint8_t motor_left_pin_ = 0;
		uint8_t motor_left_pwm_pin_ = 0;
		uint8_t motor_right_pin_ = 0;
		uint8_t motor_right_pwm_pin_ = 0;
		uint8_t motor_enable_pin_ = 0;
};