const uint8_t MOTOR_MAX_RPM = 200;
const float WHEEL_DIAMETER = 0.07;
const float FR_WHEEL_DISTANCE = 0.889;
const float LR_WHEEL_DISTANCE = 0.125;
const uint8_t PWM_BITS = 8;