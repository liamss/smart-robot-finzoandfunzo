#include "Arduino.h"
#include "ArduinoESPSerialBridge.h"
#include "ArduinoJson.h"

void ArduinoESPSerialBridge::send_msg(const Message_t& msg) {
  Serial.print(msg.action);
  Serial.print(',');
  Serial.println(msg.data);
}

bool ArduinoESPSerialBridge::is_esp_ready(void) {
  char received_char;

  if (!Serial.available()) {
    return false;
  }

  if (Serial.read() != 's') {
    return false;
  }

  return true;
}