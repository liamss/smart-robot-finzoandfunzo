const uint8_t PIN_Motor_STBY = 3;
const uint8_t PIN_Motor_AIN_1 = 7;
const uint8_t PIN_Motor_PWMA = 5;
const uint8_t PIN_Motor_BIN_1 = 8;
const uint8_t PIN_Motor_PWMB = 6;

const uint8_t PIN_ITR20001_LEFT = A2;
const uint8_t PIN_ITR20001_MIDDLE = A1;
const uint8_t PIN_ITR20001_RIGHT = A0;

const uint8_t PIN_US_TRIG = 13;
const uint8_t PIN_US_ECHO = 12;

const uint8_t PIN_RBGLED = 4;

const uint8_t PIN_SERVO = 10;

const uint8_t PIN_IR_REMOTE_RX = 9;

const uint8_t PIN_IMU_SCL = A5;
const uint8_t PIN_IMU_SDA = A4;

void pin_setup (void);