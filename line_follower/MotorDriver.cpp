#include <Arduino.h>

#include "Kinematics.h"

#include "MotorDriver.h"

MotorDriver::MotorDriver(uint8_t l_pin, uint8_t l_pwm_pin, uint8_t r_pin, uint8_t r_pwm_pin, uint8_t enable_pin):
motor_left_pin_(l_pin),
motor_left_pwm_pin_(l_pwm_pin),
motor_right_pin_(r_pin),
motor_right_pwm_pin_(r_pwm_pin),
motor_enable_pin_(enable_pin)
{
}

void MotorDriver::init(void)
{
  pinMode(motor_enable_pin_, OUTPUT);
  pinMode(motor_left_pin_, OUTPUT);
  pinMode(motor_left_pwm_pin_, OUTPUT);
  pinMode(motor_right_pin_, OUTPUT);
  pinMode(motor_right_pwm_pin_, OUTPUT);

  digitalWrite(motor_enable_pin_, HIGH);
}

void MotorDriver::drive_motor(Kinematics kinematics, Kinematics::output output)
{
  digitalWrite(motor_enable_pin_, HIGH);

	if (output.motor1 < 0 && output.motor3 < 0) {
		digitalWrite(motor_left_pin_, LOW);
		output.motor1 = 0;
	} else {
		digitalWrite(motor_left_pin_, HIGH);
	}

	if (output.motor2 < 0 && output.motor4 < 0) {
		digitalWrite(motor_right_pin_, LOW);
		output.motor2 = 0;
	} else {
		digitalWrite(motor_right_pin_, HIGH);
	}

	analogWrite(motor_right_pwm_pin_, abs(kinematics.rpmToPWM(output.motor2)));
	analogWrite(motor_left_pwm_pin_, abs(kinematics.rpmToPWM(output.motor1)));
}

void MotorDriver::stop_motor(void)
{
  //digitalWrite(motor_enable_pin_, LOW);
  digitalWrite(motor_left_pin_, LOW);
  digitalWrite(motor_right_pin_, LOW);
  analogWrite(motor_right_pwm_pin_, 0);
  analogWrite(motor_left_pwm_pin_, 0);
}