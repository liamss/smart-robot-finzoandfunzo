#include "Arduino.h"
#include "IRArrayDriver.h"

IRArrayDriver::IRArrayDriver(uint8_t l_pin, uint8_t m_pin, uint8_t r_pin):
itr_left_pin_(l_pin),
itr_middle_pin_(m_pin),
itr_right_pin_(r_pin)
{
}

void IRArrayDriver::init(void)
{
  pinMode(itr_left_pin_, INPUT);
  pinMode(itr_middle_pin_, INPUT);
  pinMode(itr_right_pin_, INPUT);
}

void IRArrayDriver::calibrate(void)
{
  itr_values_t values;
  uint16_t min_value = this->min_value_read_;
  uint16_t max_value = this->max_value_read_;
  int i = 0;

  for (i = 0; i < 300; i++) {
    values = this->get_values();

    if (values.itr_left_value > max_value) {
      max_value = values.itr_left_value; 
    }

    if (values.itr_middle_value > max_value) {
      max_value = values.itr_middle_value;
    }

    if (values.itr_right_value > max_value) {
      max_value = values.itr_right_value;
    }

    if (values.itr_left_value < min_value) {
        min_value = values.itr_left_value; 
    }

    if (values.itr_middle_value < min_value) {
        min_value = values.itr_middle_value;
    }

    if (values.itr_right_value < min_value) {
        min_value = values.itr_right_value;
    }

    delayMicroseconds(15);
  }

  this->max_value_read_ = max_value;
  this->min_value_read_ = min_value;
}

IRArrayDriver::itr_values_t IRArrayDriver::get_values(void)
{
  itr_values_t itr_values;

  itr_values.itr_left_value = analogRead(itr_left_pin_);
  itr_values.itr_middle_value = analogRead(itr_middle_pin_);
  itr_values.itr_right_value = analogRead(itr_right_pin_);

  return itr_values;
}

IRArrayDriver::itr_norm_values_t IRArrayDriver::normalize_values(itr_values_t values, float inMin, float inMax, float outMin, float outMax) {
  itr_norm_values_t normalized_values;

  normalized_values.itr_left_value = (values.itr_left_value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
  normalized_values.itr_middle_value = (values.itr_middle_value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
  normalized_values.itr_right_value = (values.itr_right_value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;

  return normalized_values;
}

float IRArrayDriver::get_line_pos(void)
{
  itr_norm_values_t itr_norm_values;
  float line_pos = 0, total = 0, weigth_avg = 0;
  bool line_presence = false;

  itr_norm_values = normalize_values(get_values(), 0.0, 1023.0, 0.0, 1.0);

  total = itr_norm_values.itr_left_value + itr_norm_values.itr_middle_value + itr_norm_values.itr_right_value;

  weigth_avg = itr_norm_values.itr_left_value * 1 + itr_norm_values.itr_middle_value * 2 + itr_norm_values.itr_right_value * 3;

  this->num_readings_++;

  if (total >= 0.8) {
    line_pos = weigth_avg/total;
    
    this->last_line_pos_ = line_pos;
    line_presence = true;
    this->num_line_readings_++;
  }

  this->line_presence_ = line_presence;

  return this->last_line_pos_;
}

float IRArrayDriver::get_line_precision(void) {
  if (this->num_readings_ == 0) {
    return 0.0;
  }

  return float(this->num_line_readings_) / float(this->num_readings_) * 100.0;
}

bool IRArrayDriver::get_line_presence(void)
{
  return this->line_presence_;
}