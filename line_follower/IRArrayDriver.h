#ifndef IRARRAYDRIVER_H
#define IRARRAYDRIVER_H

#include "Arduino.h"

class IRArrayDriver
{
  public:
    IRArrayDriver(uint8_t, uint8_t, uint8_t);

    struct itr_values {
      uint16_t itr_left_value;
      uint16_t itr_middle_value;
      uint16_t itr_right_value;
    };

    typedef struct itr_values itr_values_t;

    struct itr_norm_values {
      float itr_left_value;
      float itr_middle_value;
      float itr_right_value;
    };

    typedef struct itr_norm_values itr_norm_values_t;

    void init(void);

    void calibrate(void);
    itr_values_t get_values(void);
    itr_norm_values_t normalize_values(itr_values_t, float, float, float, float);

    float get_line_pos(void);
    bool get_line_presence(void);
    float get_line_precision(void);

  private:
    uint8_t itr_left_pin_ = 0;
    uint8_t itr_middle_pin_ = 0;
    uint8_t itr_right_pin_ = 0;
    uint16_t min_value_read_ = 1023;
    uint16_t max_value_read_ = 0;
    float last_line_pos_;
    bool line_presence_;
    uint32_t num_readings_ = 0;
    uint32_t num_line_readings_ = 0;
    uint32_t good_line_readings_ = 0;
};

#endif
