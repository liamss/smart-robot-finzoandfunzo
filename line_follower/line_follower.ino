#include <Arduino_FreeRTOS.h>

#include "FastLED.h"
#include "IRArrayDriver.h"
#include "Kinematics.h"
#include "MotorDriver.h"
#include <PID_v1.h>
#include <Servo.h>
#include <NewPing.h>
#include <queue.h>
#include "ArduinoESPSerialBridge.h"

#include "PinConfig.h"
#include "LEDConfig.h"
#include "KinematicsConfig.h"
#include "PIDConfig.h"

#include "LEDUtils.h"

IRArrayDriver ir_array_driver(PIN_ITR20001_LEFT, PIN_ITR20001_MIDDLE, PIN_ITR20001_RIGHT);
MotorDriver motor_driver(PIN_Motor_AIN_1, PIN_Motor_PWMA, PIN_Motor_BIN_1, PIN_Motor_PWMB, PIN_Motor_STBY);
Kinematics kinematics(MOTOR_MAX_RPM, WHEEL_DIAMETER, FR_WHEEL_DISTANCE, LR_WHEEL_DISTANCE, PWM_BITS);
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, REVERSE);
Servo myServo;
CRGB leds[NUM_LEDS];
NewPing sonar(PIN_US_TRIG, PIN_US_ECHO, 200);
ArduinoESPSerialBridge serial_bridge;
ArduinoESPSerialBridge::Message_t msg;

QueueHandle_t color_queue;

TaskHandle_t line_follower_task_handle;
TaskHandle_t debug_led_task_handle;

unsigned long start_time;

void TaskLineFollower( void *pvParameters );
void TaskDebugLED( void *pvParameters );
void TaskUltrasonicRead(void *pvParameters);

void setup() {
  color_queue = xQueueCreate(5, sizeof(uint32_t));
  uint8_t led_value = 5;
  int8_t led_value_increment = 5;

  xTaskCreate(
    TaskLineFollower
    ,  "LineFollower"
    ,  256  // Stack size
    ,  NULL
    ,  4  // Priority
    ,  &line_follower_task_handle );
  
  xTaskCreate(
    TaskDebugLED
    ,  "DebugLED"
    ,  128  // Stack size
    ,  NULL
    ,  1  // Priority
    ,  &debug_led_task_handle );

  xTaskCreate(
    TaskUltrasonicRead
    , "UltrasonicRead"
    , 128  // Stack size
    , NULL
    , 3  // Priority
    , NULL);
  
  Serial.begin(115200);

  myServo.attach(PIN_SERVO);
  myServo.write(100);

  FastLED.addLeds<NEOPIXEL, PIN_RBGLED>(leds, NUM_LEDS);
  FastLED.setBrightness(20);

  motor_driver.init();

  ir_array_driver.init();
  ir_array_driver.calibrate();

  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(pid_min_output, pid_max_output);

  while (!serial_bridge.is_esp_ready()) {
    FastLED.showColor(Color(led_value, led_value, led_value));

    if (led_value >= 255 || led_value <= 0) {
      led_value_increment *= -1;
    }

    led_value += led_value_increment;

    delay(10);
  }

  memset(&msg, 0, sizeof(ArduinoESPSerialBridge::Message_t));

  msg.action = ArduinoESPSerialBridge::Actions::START_LAP;

  serial_bridge.send_msg(msg);

  start_time = millis();
}

void loop() {

}

void TaskLineFollower(void *pvParameters)  // This is a task.
{
  (void) pvParameters;

  float linear_vel_x = 0.0;
  float max_linear_vel = 0.8;
  float min_linear_vel = 0.4;
  uint32_t color;
  bool line_presence;

  for (;;) {
    Input = ir_array_driver.get_line_pos();

    if (!ir_array_driver.get_line_presence()) {
      myPID.SetTunings(KpR, KiR, KdR);
      color = Color(255, 0, 0);
      linear_vel_x = 0.1;
      if (line_presence) {
        line_presence = false;
        msg.action = ArduinoESPSerialBridge::Actions::LINE_LOST;
        serial_bridge.send_msg(msg);
        msg.action = ArduinoESPSerialBridge::Actions::INIT_LINE_SEARCH;
        serial_bridge.send_msg(msg);
      }
    } else {
      myPID.SetTunings(Kp, Ki, Kd);
      color = Color(0, 255, 0);
      linear_vel_x = 0.4;
      if (!line_presence) {
        line_presence = true;
        msg.action = ArduinoESPSerialBridge::Actions::LINE_FOUND;
        serial_bridge.send_msg(msg);
        msg.action = ArduinoESPSerialBridge::Actions::STOP_LINE_SEARCH;
        serial_bridge.send_msg(msg);
      }
    }

    xQueueSend(color_queue, &color, 0);

    myPID.Compute();

    motor_driver.drive_motor(kinematics, kinematics.getRPM(linear_vel_x, 0.0, Output));

    vTaskDelay(1);
  }
}

void TaskDebugLED(void *pvParameters)
{
  (void) pvParameters;

  uint32_t color;

  for (;;) {
    if (xQueueReceive(color_queue, &color, portMAX_DELAY))
    {
      FastLED.showColor(color);
    }
  }
}

void TaskUltrasonicRead(void *pvParameters) {
  (void)pvParameters;

  TickType_t xLastWakeTime;
  unsigned long distance;
  uint32_t color;

  for (;;) {
    xLastWakeTime = xTaskGetTickCount();

    distance = sonar.ping_cm();

    xTaskDelayUntil( &xLastWakeTime, ( 4 / portTICK_PERIOD_MS ) );

    if (distance == 0.0) {
      xTaskDelayUntil( &xLastWakeTime, ( 30 / portTICK_PERIOD_MS ) );
      continue;
    }

    if (distance <= 8.0) {

      vTaskDelete(line_follower_task_handle);
      motor_driver.stop_motor();

      color = Color(0,0,255);

      xQueueSend(color_queue, &color, 0);

      memset(&msg, 0, sizeof(ArduinoESPSerialBridge::Message_t));

      msg.action = ArduinoESPSerialBridge::Actions::OBSTACLE_DETECTED;
      msg.data = float(sonar.ping_cm());

      serial_bridge.send_msg(msg);

      memset(&msg, 0, sizeof(ArduinoESPSerialBridge::Message_t));

      msg.action = ArduinoESPSerialBridge::Actions::END_LAP;
      msg.data = float(millis() - start_time);

      serial_bridge.send_msg(msg);

      memset(&msg, 0, sizeof(ArduinoESPSerialBridge::Message_t));

      msg.action = ArduinoESPSerialBridge::Actions::VISIBLE_LINE;
      msg.data = ir_array_driver.get_line_precision();

      serial_bridge.send_msg(msg);

      vTaskDelete(NULL);
    }

    xTaskDelayUntil( &xLastWakeTime, ( 30 / portTICK_PERIOD_MS ) );
  }
}

