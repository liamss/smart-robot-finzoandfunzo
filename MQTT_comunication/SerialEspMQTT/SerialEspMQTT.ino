#include <WiFi.h>
#include "Adafruit_MQTT.h"
#include "Adafruit_MQTT_Client.h"
#include <ArduinoJson.h>
#include "ArduinoESPSerialBridge.h"
// WiFi credentials with eduroam
#define EAP_ANONYMOUS_IDENTITY "20220719anonymous@urjc.es" // leave as it is
#define EAP_IDENTITY ""    // Use your URJC email
#define EAP_PASSWORD ""            // User your URJC password
#define EAP_USERNAME ""    // Use your URJC email

// SSID NAME
const char* ssid = "eduroam"; // eduroam SSID

// MQTT Server information
#define MQTT_SERVER "193.147.53.2"
#define MQTT_PORT 21883
#define MQTT_USERNAME ""
#define MQTT_PASSWORD ""

// MQTT topics
#define MQTT_TOPIC "/SETR/2023/2/"

// Define specific pins for Serial2.
#define RXD2 33
#define TXD2 4

String receiveBuff;

// Create an instance of the WiFi client
WiFiClient client;

// Setup the MQTT client
Adafruit_MQTT_Client mqtt(&client, MQTT_SERVER, MQTT_PORT, MQTT_USERNAME, MQTT_PASSWORD);

unsigned long start_time = 0;
unsigned long prev_time = 4000;
bool pinging = false;

void connectToWiFi() {
  Serial.print("Connecting to WiFi");
  WiFi.begin(ssid, WPA2_AUTH_PEAP, EAP_IDENTITY, EAP_USERNAME, EAP_PASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("\nConnected to WiFi");
}

void connectToMQTT() {
  Serial.println("Connecting to MQTT");
  int8_t ret;
  while ((ret = mqtt.connect()) != 0) {
    Serial.println(mqtt.connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    delay(5000);
  }
  Serial.println("Connected to MQTT");
}

void processAction(ArduinoESPSerialBridge::Message_t msg) {
  // Create a JSON object
  StaticJsonDocument<200> jsonDoc;

  // Add common data to the JSON object
  jsonDoc["team_name"] = "Finzo&Funzo";
  jsonDoc["id"] = "2";

  // Add specific data based on action
  switch (msg.action) {
    case 1:
      jsonDoc["action"] = "START_LAP";
      start_time = millis();
      pinging = true;
      break;
    case 2:
      jsonDoc["action"] = "END_LAP";
      jsonDoc["time"] = msg.data; // Replace with the actual time value
      pinging = false;
      break;
    case 3:
      jsonDoc["action"] = "OBSTACLE_DETECTED";
      jsonDoc["distance"] = msg.data; // Replace with the actual distance value
      break;
    case 4:
      jsonDoc["action"] = "LINE_LOST";
      break;
    case 5:
      jsonDoc["action"] = "PING";
      jsonDoc["time"] = msg.data; // Replace with the actual time value
      break;
    case 6:
      jsonDoc["action"] = "INIT_LINE_SEARCH";
      break;
    case 7:
      jsonDoc["action"] = "STOP_LINE_SEARCH";
      break;
    case 8:
      jsonDoc["action"] = "LINE_FOUND";
      break;
    case 9:
      jsonDoc["action"] = "VISIBLE_LINE";
      jsonDoc["value"] = msg.data; // Replace with the actual value
      break;
    default:
      // Invalid action
      Serial.println("Invalid action");
  }

  // Print the received JSON
  Serial.println("Received JSON:");
  Serial.print("\t\"team_name\": ");
  Serial.println(jsonDoc["team_name"].as<String>());
  Serial.print("\t\"id\": ");
  Serial.println(jsonDoc["id"].as<String>());
  Serial.print("\t\"action\": ");
  Serial.println(jsonDoc["action"].as<String>());
  if (jsonDoc.containsKey("time")) {
    Serial.print("\t\"time\": ");
    Serial.println(jsonDoc["time"].as<long>());
  }
  if (jsonDoc.containsKey("distance")) {
    Serial.print("\t\"distance\": ");
    Serial.println(jsonDoc["distance"].as<int>());
  }
  if (jsonDoc.containsKey("value")) {
    Serial.print("\t\"value\": ");
    Serial.println(jsonDoc["value"].as<float>());
  }

  // Publish the formatted JSON to the MQTT topic
  String formattedJson;
  serializeJsonPretty(jsonDoc, formattedJson);
  Adafruit_MQTT_Publish mqttPublish(&mqtt, MQTT_TOPIC);
  mqttPublish.publish(formattedJson.c_str());
}

ArduinoESPSerialBridge::Message_t msg;

void setup() {
  Serial.begin(9600);
  Serial2.begin(115200, SERIAL_8N1, RXD2, TXD2);

  connectToWiFi();
}

void processReceivedData() {
  if (Serial2.available()) {
    String receivedData = Serial2.readStringUntil('\n');
    ArduinoESPSerialBridge::Message_t msg;

    char *strtokIndx;
    char tempData[receivedData.length() + 1]; // Create a temporary array to store the received string

    receivedData.toCharArray(tempData, sizeof(tempData)); // Convert String to a char array

    strtokIndx = strtok(tempData, ",");
    msg.action = static_cast<ArduinoESPSerialBridge::Actions>(atoi(strtokIndx));

    strtokIndx = strtok(NULL, ",");
    msg.data = atof(strtokIndx); // Convert this part to a float

    processAction(msg);
  }
}

void loop() {
  processReceivedData();

  // Maintain the MQTT connection
  if (!mqtt.connected()) {
    connectToMQTT();
    if(mqtt.connected()) {
      Serial2.print('s');
    }
  }

  if (millis() - prev_time >= 4000 && pinging) {
    prev_time = millis();
    msg.action = ArduinoESPSerialBridge::Actions::PING;
    msg.data = float(millis() - start_time);
    processAction(msg);
  } 
}

