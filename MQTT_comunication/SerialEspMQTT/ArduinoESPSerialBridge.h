class ArduinoESPSerialBridge
{
  public:

    enum Actions {
      START_LAP = 1,
      END_LAP,
      OBSTACLE_DETECTED,
      LINE_LOST,
      PING,
      INIT_LINE_SEARCH,
      STOP_LINE_SEARCH,
      LINE_FOUND,
      VISIBLE_LINE,
    };

    typedef enum Actions Actions;

    struct Message {
      Actions action;
      float data;
    };

    typedef struct Message Message_t;

    void send_msg(const Message_t& msg);
    bool is_esp_ready(void);
};