<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.eif.urjc.es/liamss/smart-robot-finzoandfunzo">
  </a>

  <h3 align="center">Smart Robot Finzo&Funzo</h3>

  <p align="center">
    Line-following robot
    <br />
    <a href="https://gitlab.etsit.urjc.es/roberto.calvo/setr/-/wikis/P4FollowLine"><strong>Documentation of the project »</strong></a>
    <br />

  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#video">Video</a></li>
    <li>
      <a href="#explanation-of-code">Explanation of code</a>
      <ul>
        <li><a href="#freertos">FreeRTOS</a></li>
        <li><a href="#communication">Communication</a></li>
        <li><a href="#following-line">Following line</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

[![Elegoo smart robot][product-screenshot]](https://www.elegoo.com/en-es/products/elegoo-smart-robot-car-kit-v-4-0)

This project features a line-following robot created using a elegoo smartrobot. The robot utilizes Arduino for control, incorporating sensors to follow a designated path. The communication backbone involves serial communication between Arduino and an ESP32 microcontroller, leveraging MQTT (Message Queuing Telemetry Transport) for seamless data exchange.



### Built With

This project is crafted using cutting-edge technologies and relies on fundamental frameworks such as Arduino for microcontroller programming and control. The seamless integration of sensors and motor control is made possible through the robust capabilities of the Arduino framework, ensuring a solid foundation for the line-following robot's functionality.


<!-- GETTING STARTED -->
## Getting Started

This section provides instructions on setting up your Line-Following Robot project locally. Follow these simple steps to get a local copy up and running.

### Prerequisites

Make sure you have the following prerequisites installed:

[Arduino IDE](https://support.arduino.cc/hc/en-us/articles/360019833020-Download-and-install-Arduino-IDE) for uploading code to the robot.

### Installation

Follow these steps to install and set up your Line-Following Robot:

1. Clone the repo
   ```sh
   git clone https://gitlab.eif.urjc.es/liamss/smart-robot-finzoandfunzo.git
   ```
2. Install kibraries in you Arduino IDE
   ```sh
    #include <WiFi.h>
    #include <PID_v1.h>
    #include <Servo.h>
    #include <NewPing.h>
    #include <queue.h>
   ```
3. Configure Wifi

Open the ESP32 code "SerialEspMQTT.ino" and edit the following lines:

* Using your local wifi:
  ```c
    // WiFi credentials
  const char *ssid = "SSID" // Use the name of your wifi;
  const char *password = "PASSWORD" // Use the password of  your wifi;

  //Set up the wifi like this:
  WiFi.begin(ssid, password);

  ```

* Using eduroam:

  ```c
  // WiFi credentials with eduroam
  #define EAP_ANONYMOUS_IDENTITY "20220719anonymous@urjc.es" // leave as it is
  #define EAP_IDENTITY "USER@urjc.es"    // Use your URJC email
  #define EAP_PASSWORD "PASSWORD"            // User your URJC password
  #define EAP_USERNAME "USER@urjc.es"    // Use your URJC email

  // SSID NAME
  const char* ssid = "eduroam"; // eduroam SSID


  //Set up the wifi like this:
  WiFi.begin(ssid, WPA2_AUTH_PEAP, EAP_IDENTITY, EAP_USERNAME, EAP_PASSWORD);
  ```


4. Configure MQTT

Open the ESP32 code "SerialEspMQTT.ino" and edit the following lines:

```c
// MQTT Server information
#define MQTT_SERVER "193.147.53.2"
#define MQTT_PORT 21883
#define MQTT_USERNAME ""
#define MQTT_PASSWORD ""

// MQTT topics
#define MQTT_TOPIC "/SETR/2023/2/"

```


<!-- USAGE EXAMPLES -->
## Usage

1. Upload Code:

* Upload line_follower.ino code to the Arduino microcontroller.
* Upload SerialEspMQTT.ino code to the ESP32 microcontroller.


2. Switch to CAM ID:

Once the code is uploaded, locate the switch or setting on the Arduino board that corresponds to the CAM ID (Camera ID).
Adjust the switch to the desired CAM ID. This setting is crucial for the communication with the two microcontrollers.

3. Watch the comunication:

  Open a terminal and write:

  ```sh
  mosquitto_sub -v -h 193.147.53.2 -p 21883 -t /SETR/2023/$ID_EQUIPO/
  ```
  Test the server like this:
  ```sh
  mosquitto_pub -h 193.147.53.2 -p 21883 -t /SETR/2023/$ID_EQUIPO/ -m "hello world!"

  ```

4. Operate the Line-Following Robot:

Power up the line-following robot, ensuring that both the ESP32 and Arduino are connected and powered.
Place the robot on a track or surface with a visible black line.
Observe the robot's movements as it follows the line based on sensor inputs and control signals from the Arduino.


<!-- Video EXAMPLES -->
## Video

Watch here a video of the project!

![Line-Following Robot Demo](https://gitlab.eif.urjc.es/liamss/smart-robot-finzoandfunzo/-/raw/main/imgs/ezgif.com-video-to-gif-converted.gif)



<!-- EXPLANATION OF THE CODE -->
## Explanation of code

### FreeRTOS

In our Line-Following Robot project, FreeRTOS is implemented to enable multi-tasking capabilities, allowing concurrent execution of various functions. The advantages of using FreeRTOS include improved code organization, real-time responsiveness, and efficient task management.

Three tasks are created using FreeRTOS: TaskLineFollower, TaskDebugLED, and TaskUltrasonicRead. These tasks operate independently, handling line-following logic, LED display, and ultrasonic sensor readings simultaneously.

Tasks are assigned different priorities to manage their execution order. In this example, TaskLineFollower has the highest priority (4), followed by TaskUltrasonicRead (3) and TaskDebugLED (1). Priority levels determine the order in which tasks are scheduled for execution.

* TaskLineFollower:
Implements the line-following algorithm, adjusting the robot's movements based on infrared sensor input.
Communicates with the Arduino using the ArduinoESPSerialBridge for sending messages related to line presence, line loss, and line detection.

* TaskDebugLED:
Manages LED color display based on information received through a color queue.
Provides visual feedback using FastLED library.

* TaskUltrasonicRead:
Periodically reads data from an ultrasonic sensor to detect obstacles.
Sends messages to the Arduino using the ArduinoESPSerialBridge when obstacles are detected, and lap-related events occur.

We use communication between Tasks, the color queue is used to pass LED color information from TaskLineFollower to TaskDebugLED, enabling visual feedback based on the line-following status.

By leveraging FreeRTOS in our Arduino code, we enhance the overall efficiency, responsiveness, and modularity of the Line-Following Robot project. Each task operates independently, contributing to a seamless and well-organized robotic system.


### Communication

The Arduino microcontroller prints a message and the ESP32 microcontroller is equipped to receive messages through the Serial2 interface and subsequently publish them using MQTT.

Serial2 is set up with specific pins (RXD2 and TXD2) to establish communication with other devices, possibly the Arduino.

The connectToWiFi() function connects the ESP32 to the specified Wi-Fi network, and connectToMQTT() establishes a connection to the MQTT broker. These connections are crucial for both sending and receiving messages.

The processReceivedData() function listens for data on the Serial2 interface. When data is received, it is parsed and converted into a structured message (ArduinoESPSerialBridge::Message_t), including an action and data payload.

The processAction() function takes the parsed message and constructs a JSON object using the ArduinoJson library. The JSON object includes common information such as team name and ID, along with specific data based on the received action. Actions include START_LAP, END_LAP, OBSTACLE_DETECTED, LINE_LOST, PING, INIT_LINE_SEARCH, STOP_LINE_SEARCH, LINE_FOUND, and VISIBLE_LINE.

The formatted JSON is printed to the Serial Monitor for debugging and monitoring purposes. The printed information includes team name, ID, action, and relevant data (time, distance, or value). The formatted JSON is then published to the specified MQTT topic using the Adafruit_MQTT_Publish class.


### Following line

The procedure could be splitted in two main phases:

1. **Detection**.
2. **Actuation**.

Both phases will be covered individually.

#### Detection.

Line detection is accomplished using an array of IR sensors, a common approach in such projects. Some manufacturers already provide libraries for reading these sensors, and while we've drawn inspiration from their ideas, we've developed our own implementation.

Each sensor produces an analog voltage that the microcontroller measures. Initially, this measurement only indicated how dark the floor was beneath the sensor. However, our goal was to determine the line's location.

To achieve this, we combined information from all sensors. We assigned a weight to each sensor, corresponding to its physical position in the board module. By normalizing the values both before and after calculations, we derived a result ranging from 1.0 to 3.0, as we're using three sensors.

A value of 1.0 indicates that the line is directly under sensor 1, while a value of 3.0 means it's under sensor 3. Any value between these extremes signifies the line's relative position to the board. To properly track the line, we aim to get this value as close to 2.0 as possible, as it corresponds to the middle sensor's index.

Starting the indexes at 1 gives us an advantage: a reading of 0 indicates that none of the sensors detect the line at all, which is also a useful indication.

#### Actuation.

The robot is equipped with two pairs of motors. However, controlling them directly via raw PWM isn't ideal. That's why we opted to use a kinematics model tailored for differential robots.

This approach allows us to input a more user-friendly value, such as X.X m/s for forward motion, into our driver. The driver then computes the necessary duty cycle for each motor pair to achieve that specific linear speed. Precise maneuvers require consideration of the robot's physical dimensions, which we did. However, due to the lack of feedback hardware, a tiny error is acumulated over long distances or time.

##### PID

Following the progress made in Detection and Actuation, the PID controller now takes as input the error between the setpoint (which is 2.0) and the current detection value. If the line is lost (i.e., the IR array driver reads 0.0), the last recorded value is utilized in an attempt to regain tracking of the line. The output directly determines the angular velocity, which is then converted for the motor drivers' use.

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- CONTACT -->
## Contact

Liam Saborido Sueiro - l.saborido.2021@alumnos.urjc.es

Diego García Currás - d.garciac.2021@alumnos.urjc.es




<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[product-screenshot]: https://gitlab.eif.urjc.es/liamss/smart-robot-finzoandfunzo/-/raw/main/imgs/elegoo-smart-robot-car-kit.jpg
